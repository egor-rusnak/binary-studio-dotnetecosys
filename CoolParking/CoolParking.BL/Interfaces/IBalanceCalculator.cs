﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.BL.Interfaces
{
    public interface IBalanceCalculator
    {
        decimal Calculate(decimal balance, decimal withdraw, decimal penalty = 0);
    }
}
