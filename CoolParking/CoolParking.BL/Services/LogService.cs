﻿using CoolParking.BL.Interfaces;
using System;
using System.IO;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public string LogPath { get; init; }
        public LogService(string logPath)
        {
            LogPath = logPath;
        }

        public string Read()
        {
            try
            {
                using(StreamReader stream = new StreamReader(File.OpenRead(LogPath)))
                {
                    return stream.ReadToEnd();
                }
            }
            catch (IOException ex)
            {
                throw new InvalidOperationException("Log file reading Error: "+ex.Message);
            }
        }

        public void Write(string logInfo)
        {
            if (string.IsNullOrWhiteSpace(logInfo)) 
                return;
            try
            {
                using (StreamWriter stream = new StreamWriter(LogPath,true))
                {
                    stream.WriteLine(logInfo);
                }
            }
            catch(IOException ex)
            {
                throw new InvalidOperationException("Error writing file :" + ex.Message);
            }
        }
    }
}