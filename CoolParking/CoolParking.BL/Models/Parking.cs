﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        private static Parking _parking;
        public static Parking Instance
        {
            get
            {
                if (_parking == null) _parking = new();
                return _parking;
            }
        }

        public decimal Balance { get; internal set; }
        public List<Vehicle> Vehicles { get; internal set; } = new List<Vehicle>();
        public int Capacity { get; internal set; }

        private Parking() { }


    }
}
