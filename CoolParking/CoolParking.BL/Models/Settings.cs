﻿using System;
using System.IO;
using System.Threading;
using System.Xml;
using System.Xml.Serialization;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static readonly string settingsFile = Directory.GetCurrentDirectory() + @"/Settings.xml";
        public static ParkingConfiguration Configuration { get; private set; } = ParkingConfiguration.GetDefaultConfiguration();

        public static bool HasSavedSettings() 
            => File.Exists(settingsFile);

        public static void SaveSettings()
        {
            try
            {
                using (var fs = new FileStream(settingsFile, FileMode.Create))
                {
                    var serializer = new XmlSerializer(typeof(ParkingConfiguration));
                    serializer.Serialize(fs, Configuration);
                }
            }
            catch(IOException ex)
            {
                throw new InvalidOperationException("Can't save file, but default props will be setted up! Exception: ", ex);
            }
        }

        public static void LoadSettings()
        {
            try
            {
                using (var fs = File.OpenRead(settingsFile))
                {
                    var serializer = new XmlSerializer(typeof(ParkingConfiguration));
                    Configuration = (ParkingConfiguration)serializer.Deserialize(fs);
                }
            }
            catch(InvalidOperationException ex)
            {
                SetDefaultSettingsByError(ex);
            }
        }

        private static void SetDefaultSettingsByError(Exception ex)
        {
            Configuration = ParkingConfiguration.GetDefaultConfiguration();
            SaveSettings();

            throw new InvalidOperationException("Bad file in settings! A new file was created!", ex);
        }
    }
}