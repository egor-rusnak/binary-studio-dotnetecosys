﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Models
{
    public class ParkingBalanceCalculator : IBalanceCalculator
    {
        public decimal Calculate(decimal balance, decimal withdraw, decimal penalty = 0)
        {
            if (balance < withdraw && balance > 0)
            {
                var difference = -balance + withdraw;
                withdraw += difference * penalty - difference;
            }
            else if (balance <= 0)
                withdraw *= penalty;

            return withdraw;
        }
    }
}
