﻿using System;

public struct TransactionInfo
{
    public decimal Sum { get; init; }
    public decimal Balance { get; init; }
    public DateTimeOffset DateAndTime { get; init; }
    public string VechicleId { get; init; }

    public TransactionInfo(string vehicleId, decimal sum, decimal balance)
    {
        VechicleId = vehicleId;
        Sum = sum;
        DateAndTime = System.DateTime.Now;
        Balance = balance;
    }

    public override string ToString()
        => $"TIME: {DateAndTime}; ID: {VechicleId}; BALANCE: {Balance}, WITHDRAW: {Sum}";
}