﻿using CoolParking.BL.Interfaces;
using CoolParking.ConsoleUI.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.ConsoleUI.Commands
{
    public class GetFreePlaces : IParkingCommand
    {
        public string Description { get; init; } = "Get Free Places";

        public void Execute(IParkingService parking, TextReader rd, TextWriter wr)
            => wr.WriteLine($"Free places: {parking.GetFreePlaces()} of {parking.GetCapacity()}");
    }
}
