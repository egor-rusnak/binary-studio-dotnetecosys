﻿using CoolParking.BL.Interfaces;
using CoolParking.ConsoleUI.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace CoolParking.ConsoleUI.Commands
{
    class SeeCurrentTransactions : IParkingCommand
    {
        public string Description { get; init; } = "Show current transactions";

        public void Execute(IParkingService parking, TextReader rd, TextWriter wr)
        {
            var transactions = parking.GetLastParkingTransactions();

            if (transactions.Length > 0)
                wr.WriteLine("Transactions:\n"+string.Join("\n", transactions));
            else 
                wr.WriteLine("No transactions in the log");
        }
    }
}
