﻿using CoolParking.BL.Interfaces;
using CoolParking.ConsoleUI.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.ConsoleUI.Commands
{
    public class ShowTransactionHistory : IParkingCommand
    {
        public string Description { get; init; } = "Show transactions history";

        public void Execute(IParkingService parking, TextReader rd, TextWriter wr)
        {
            try
            {
                wr.WriteLine($"Past transactions:\n{parking.ReadFromLog()}");
            }
            catch (InvalidOperationException ex)
            {
                wr.WriteLine($"Error: {ex.Message}");
            }
        }
    }
}
