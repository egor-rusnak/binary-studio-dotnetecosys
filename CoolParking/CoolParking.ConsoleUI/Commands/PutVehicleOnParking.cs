﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.ConsoleUI.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;
using static CoolParking.ConsoleUI.Input;

namespace CoolParking.ConsoleUI.Commands
{
    public class PutVehicleOnParking : IParkingCommand
    {
        public string Description { get; init; } = "Put vehicle on Parking";

        public void Execute(IParkingService parking, TextReader rd, TextWriter wr)
        {
            try 
            {
                parking.AddVehicle(new Vehicle(ReadId(wr,rd), ReadType(wr,rd), ReadSum(wr,rd)));
                wr.WriteLine("Vehicle was successfully added on parking!");
            }
            catch (ArgumentException e) 
            { 
                wr.WriteLine($"Error: {e.Message}"); 
            }
        }
    }
}
