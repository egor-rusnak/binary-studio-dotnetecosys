﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.ConsoleUI.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CoolParking.ConsoleUI.Input;


namespace CoolParking.ConsoleUI.Commands
{
    public class TopUpVehicleBalance : IParkingCommand
    {
        public string Description { get; init; } = "Top up vehicle Balance";

        public void Execute(IParkingService parking, TextReader rd, TextWriter wr)
        {
            if (!ShowVehiclesIfExists(parking, rd, wr)) 
                return;

            try 
            { 
                parking.TopUpVehicle(ReadId(wr,rd), ReadSum(wr,rd));
                wr.WriteLine("Balance was topped up successfully!");
            }
            catch(ArgumentException e) 
            { 
                wr.WriteLine($"Error: {e.Message}"); 
            }
        }
        private bool ShowVehiclesIfExists(IParkingService parking, TextReader rd, TextWriter wr)
        {
            new ShowVehiclesOnParking().Execute(parking, rd, wr);

            if (parking.GetVehicles().Count == 0) 
                return false;
            else 
                return true;
        }
    }
}
