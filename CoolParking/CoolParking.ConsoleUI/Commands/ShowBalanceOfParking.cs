﻿using CoolParking.BL.Interfaces;
using CoolParking.ConsoleUI.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.ConsoleUI.Commands
{
    public class ShowBalanceOfParking : IParkingCommand
    {
        public string Description { get; init; } = "Show balance of parking";

        public void Execute(IParkingService parking, TextReader rd, TextWriter wr)
            => wr.WriteLine($"Parking balance is {parking.GetBalance():C2}");
    }
}
