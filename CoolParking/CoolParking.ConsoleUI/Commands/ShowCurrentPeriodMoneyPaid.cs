﻿using CoolParking.BL.Interfaces;
using CoolParking.ConsoleUI.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.ConsoleUI.Commands
{
    public class ShowCurrentPeriodMoneyPaid : IParkingCommand
    {
        public string Description { get; init; } = "Show current period money paid";

        public void Execute(IParkingService parking, TextReader rd, TextWriter wr)
        {
            var sum = parking.GetLastParkingTransactions().Sum(e => e.Sum);
            wr.WriteLine($"Current period sum is {sum:C2}");
        }
    }
}
