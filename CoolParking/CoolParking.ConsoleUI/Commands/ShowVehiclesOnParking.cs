﻿using CoolParking.BL.Interfaces;
using CoolParking.ConsoleUI.Interfaces;
using Microsoft.TeamFoundation.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.ConsoleUI.Commands
{
    public class ShowVehiclesOnParking : IParkingCommand
    {
        public string Description { get; init; } = "Show vehicles on parking";

        public void Execute(IParkingService parking, TextReader rd, TextWriter wr)
        {
            var result = string.Join("\n",parking.GetVehicles().Select(v => $"Id: {v.Id}; Balance: {v.Balance.ToString("C2")}; Type: {v.Type}"));
            
            if (!result.IsNullOrEmpty())
                wr.WriteLine($"Vehicles on parking:\n{result}");
            else 
                wr.WriteLine("No vehicles on parking for now!");
        }
    }
}
