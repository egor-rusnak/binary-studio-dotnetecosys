﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.ConsoleUI.Interfaces
{
    public interface IParkingCommand
    {
        string Description { get; init; }
        void Execute(IParkingService parking,TextReader rd, TextWriter wr);
    }
}
