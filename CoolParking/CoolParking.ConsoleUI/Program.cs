﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using System;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;

namespace CoolParking.ConsoleUI
{
    class Program
    {
        static void Main(string[] args)
        {
            var app = new ParkingApp();
            app.StartParking();
        }
    }
}
