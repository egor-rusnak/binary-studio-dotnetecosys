﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using CoolParking.ConsoleUI.Commands;
using CoolParking.ConsoleUI.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CoolParking.ConsoleUI
{
    public class ParkingApp :IDisposable
    {
        private const string ExitCommand = "exit";
        private const string UpdateSettingsCommand = "update-settings";

        private readonly TextWriter _writer = Console.Out;
        private readonly TextReader _reader = Console.In;
        private string logPath = Directory.GetCurrentDirectory() + "/Transactions.txt";
        private IReadOnlyList<IParkingCommand> commands { get; set; } = new List<IParkingCommand>(new IParkingCommand[]
        {
            new ShowVehiclesOnParking(),
            new PutVehicleOnParking(),
            new TakeOffVehicleFromParking(),
            new ShowBalanceOfParking(),
            new SeeCurrentTransactions(),
            new GetFreePlaces(),
            new ShowCurrentPeriodMoneyPaid(),
            new ShowTransactionHistory(),
            new TopUpVehicleBalance()
        });

        private IParkingService _parking;
        private ITimerService _withdrawTimer;
        private ITimerService _logTimer;

        public ParkingApp() { }

        public ParkingApp(string logPath) : this() 
            => this.logPath = logPath;

        public ParkingApp(string logPath, TextWriter wr, TextReader rd) : this(logPath)
        {
            _writer = wr;
            _reader = rd;
        }

        public void StartParking()
        {
            ShowWelcomeMessage();
            InitializeParking();
            RunCommandLoop();
        }

        private void ShowWelcomeMessage()
        {
            _writer.WriteLine("Welcome to Cool Parking!");
            _writer.WriteLine("This app helps you to manage you parking with interface!");
        }

        private void InitializeParking()
        {
            LoadConfiguration();
            _parking?.Dispose();
            _parking = BuildParkingWithServices();
            _withdrawTimer?.Start();
            _logTimer?.Start();
        }

        private void LoadConfiguration()
        {
            try
            {
                _writer.WriteLine("Reading your settings!");
                Settings.LoadSettings();
            }
            catch (InvalidOperationException e)
            {
                _writer.WriteLine($"Error: {e.Message}");
            }
        }

        private ParkingService BuildParkingWithServices()
        {
            _withdrawTimer = new TimerService();
            _logTimer = new TimerService();

            return new ParkingService(_withdrawTimer, _logTimer,new LogService(logPath));
        }

        private void RunCommandLoop()
        {
            while (true)
            {
                ShowMenuMessage();

                string input = _reader.ReadLine();
                if (int.TryParse(input, out int commandIndex) && commandIndex <= commands.Count && commandIndex > 0)
                    ExecuteCommand(commands.ElementAt(commandIndex - 1));
                else if (input == ExitCommand)
                {
                    Stop();
                    break;
                }
                else if (input == UpdateSettingsCommand)
                    InitializeParking();
                else
                    _writer.WriteLine("Input error, Try Again!");
            };
        }

        private void ShowMenuMessage()
        {
            Input.ShowMenu(commands.Select(c => c.Description), _writer);

            _writer.WriteLine($"{UpdateSettingsCommand}: Update settings from file (removes current logs, vehicles and balance)");
            _writer.WriteLine($"{ExitCommand}: Exit");
            _writer.Write("Write a command >> ");
        }

        private void ExecuteCommand(IParkingCommand command)
        {
            command.Execute(_parking, _reader, _writer);
            Thread.Sleep(1000);
        }

        public void Stop()
        {
            ShowByeMessage();
            Dispose();
        }

        private void ShowByeMessage()
            => _writer.WriteLine("See you next time!");

        public void Dispose()
        {
            _parking.Dispose();
            _writer.Dispose();
            _reader.Dispose();
        }
    }
}
